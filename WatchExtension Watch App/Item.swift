//
//  Item.swift
//  WatchExtension Watch App
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation

struct Item: Identifiable {
    var id = UUID()
    let name: String
    let description: String
}

//
//  WatchExtensionApp.swift
//  WatchExtension Watch App
//
//  Created by Salvador Lopez on 28/06/23.
//

import SwiftUI
import MapKit

@main
struct WatchExtension_Watch_AppApp: App {
    @Environment(\.scenePhase) private var phase
    var body: some Scene {
        WindowGroup {
            MapContentView()
        }
        .onChange(of: phase) { newPhase in
            switch newPhase {
            case .active:
                print("App became active")
            case .inactive:
                print("App became inactive")
            case .background:
                print("App is running in the backgorund")
            default:
                print("Unknown Phase")
            }
        }
    }
}

//
//  DetailView.swift
//  WatchExtension Watch App
//
//  Created by Salvador Lopez on 28/06/23.
//

import Foundation
import SwiftUI

struct DetailView: View{
    let item: Item
    var body: some View{
        VStack{
            Text(item.name)
                .font(.largeTitle)
            Text(item.description)
                .font(.subheadline)
            Text("\(item.id)")
                .font(.subheadline)
        }
    }
}

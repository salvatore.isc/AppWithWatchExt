//
//  ContentView.swift
//  WatchExtension Watch App
//
//  Created by Salvador Lopez on 28/06/23.
//

import SwiftUI
import MapKit
import WatchKit

struct ContentView: View {
    
    @State var estadoToggle = false
    @State var valueSlider: Double = 0
    
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
            Button{
                print("Button pressed...")
            } label: {
                Text("Login")
                    .frame(width: 100, alignment: .center)
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.green)
                    .cornerRadius(50)
            }
            Button("Registro"){
                print("Resgister action...")
            }
            Toggle(isOn: $estadoToggle){
                Text("Politica de uso:")
                    .font(.subheadline)
                    .foregroundColor(.gray)
            }
            Slider(value: $valueSlider, in: 0...10, step: 1)
                .onChange(of: valueSlider) { newValue in
                    print(newValue)
                }
        }
        .padding()
    }
}


struct PickerContentView: View{
    let options = ["Opcion 1", "Opcion 2", "Opcion 3"]
    @State private var selectedOption = 0
    
    var body: some View {
        VStack{
            Text("Selecciona una opcion")
            Picker("Opciones", selection: $selectedOption){
                ForEach(0..<options.count){
                    index in
                    Text(options[index])
                }
            }
            .labelsHidden()
            .onChange(of: selectedOption) { newValue in
                print("\(options[newValue])")
            }
        }
    }
}

struct ListContentView: View{
    
    var items = [
        Item(name: "Item 1", description: "Description 1"),
        Item(name: "Item 2", description: "Description 2"),
        Item(name: "Item 3", description: "Description 3"),
        Item(name: "Item 4", description: "Description 4"),
        Item(name: "Item 5", description: "Description 5"),
        Item(name: "Item 6", description: "Description 6"),
        Item(name: "Item 7", description: "Description 7")
    ]
    
    var body: some View{
        
        NavigationView{
            List(items){
                item in
                NavigationLink(destination: DetailView(item: item)){
                    HStack{
                        Image(systemName: "figure.taichi")
                        Text(item.name)
                            .font(.subheadline)
                            .foregroundColor(.green)
                    }
                }
            }
        }
    }
}

struct MapContentView: View{
    @State private var region = MKCoordinateRegion()
    var body: some View{
        VStack{
            Map(coordinateRegion: $region)
        }
    }
}

struct WKContentView: View{
    var body: some View{
        ScrollView{
            Text(WKInterfaceDevice.current().name)
            Text(WKInterfaceDevice.current().systemName)
            Text(WKInterfaceDevice.current().systemVersion)
            Text("Ancho: \(WKInterfaceDevice.current().screenBounds.width)")
            Text("Alto: \(WKInterfaceDevice.current().screenBounds.height)")
            Text(WKInterfaceDevice.current().batteryLevel.description)
            Text("\(WKInterfaceDevice.current().waterResistanceRating.rawValue)")
            Text("\(WKInterfaceDevice.current().wristLocation.rawValue)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        WKContentView()
    }
}
